package programacion2.jala.generics;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GenericAnimalTest {

  private GenericAnimal<Cat> animal;

  @BeforeEach
  void setUp() {
    animal = new GenericAnimal<>(new Cat("Gato"));
  }

  @Test
  void getAnimal() {
    assertEquals("Gato", animal.getAnimal().getName());
  }

  @Test
  void setAnimal() {
    animal.setAnimal(new Cat("Gato2"));
    assertEquals("Gato2", animal.getAnimal().getName());
  }

  @Test
  void caminar() {
    assertEquals("Caminando", animal.caminar());
  }

  @Test
  void compareTo() {
    GenericAnimal<Cat> animal2 = new GenericAnimal<>(new Cat("Gato"));
    assertEquals(0, animal.compareTo(animal2.getAnimal()));
  }
}
