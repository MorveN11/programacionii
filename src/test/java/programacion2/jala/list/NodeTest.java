package programacion2.jala.list;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class NodeTest {

  private Node<String> node;

  @BeforeEach
  void setUp() {
    node = new Node<>("Node1");
  }

  @Test
  void testToString() {
    assertEquals("Node: Node1 -> Node: null", node.toString());
  }
}
