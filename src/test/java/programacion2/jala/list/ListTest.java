package programacion2.jala.list;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programacion2.jala.exceptions.NotFoundElementException;

class ListTest {

  private List<Integer> list;

  @BeforeEach
  void setUp() {
    list = new List<>();
  }

  @Test
  void add() {
    list.add(1);
    list.add(2);
    list.add(3);
    System.out.println(list);
  }

  @Test
  void addAtIndex() {
    list.add(1);
    list.add(2);
    assertEquals(100, list.addAtIndex(1, 100));
    assertEquals(300, list.addAtIndex(0, 300));
    assertEquals(200, list.addAtIndex(3, 200));
    assertEquals(1000, list.addAtIndex(5, 1000));
    System.out.println(list);
  }

  @Test
  void remove() {
    list.add(1);
    list.add(2);
    list.add(3);
    int removedValue = list.remove(2);
    assertEquals(2, removedValue);
    int removedValue2 = list.remove(3);
    assertEquals(3, removedValue2);
    list.add(10);
    list.add(20);
    int removedValue3 = list.remove(1);
    assertEquals(1, removedValue3);
  }

  @Test
  void size() {
    list.add(1);
    list.add(2);
    list.add(3);
    assertEquals(3, list.size());
  }

  @Test
  void update() {
    list.add(1);
    list.add(2);
    list.add(3);
    assertEquals("""
            ------- List -------
            Node: 1 -> Node: 2
            Node: 2 -> Node: 3
            Node: 3 -> Node: null
            --------------------
            """, list.toString());
    int updatedElement = list.update(3, 4);
    assertEquals(3, updatedElement);
    assertEquals("""
            ------- List -------
            Node: 1 -> Node: 2
            Node: 2 -> Node: 4
            Node: 4 -> Node: null
            --------------------
            """, list.toString());
    list.update(1, 5);
    list.update(4, 10);
    list.update(2, 35);
    assertEquals("""
            ------- List -------
            Node: 5 -> Node: 35
            Node: 35 -> Node: 10
            Node: 10 -> Node: null
            --------------------
            """, list.toString());
    assertThrows(NotFoundElementException.class, () -> list.update(90, 100));
  }

  @Test
  void updateByIndex() {
    list.add(1);
    list.add(2);
    list.add(3);
    int beforeUpdateValue = list.updateByIndex(2, 4);
    assertEquals(3, beforeUpdateValue);
    assertEquals("""
            ------- List -------
            Node: 1 -> Node: 2
            Node: 2 -> Node: 4
            Node: 4 -> Node: null
            --------------------
            """, list.toString());
    assertThrows(IndexOutOfBoundsException.class, () -> list.updateByIndex(-1, 100));
    assertThrows(IndexOutOfBoundsException.class, () -> list.updateByIndex(3, 100));
  }

  @Test
  void compareIndexes() {
    list.add(1);
    list.add(2);
    list.add(3);
    assertEquals(-1, list.compareIndexes(0, 1));
    assertEquals(1, list.compareIndexes(1, 0));
    assertEquals(0, list.compareIndexes(0, 0));
    assertThrows(IndexOutOfBoundsException.class, () -> list.compareIndexes(-1, 100));
    assertThrows(IndexOutOfBoundsException.class, () -> list.compareIndexes(3, 100));
  }

  @Test
  void isEmpty() {
    assertTrue(list.isEmpty());
    list.add(1);
    assertFalse(list.isEmpty());
  }

  @Test
  void get() {
    list.add(1);
    list.add(2);
    list.add(3);
    assertEquals(1, list.get(1));
    assertEquals(2, list.get(2));
    assertEquals(3, list.get(3));
    assertThrows(NotFoundElementException.class, () -> list.get(90));
  }

  @Test
  void getIndex() {
    list.add(1);
    list.add(2);
    list.add(3);
    assertEquals(1, list.getByIndex(0));
    assertEquals(2, list.getByIndex(1));
    assertEquals(3, list.getByIndex(2));
    assertThrows(IndexOutOfBoundsException.class, () -> list.getByIndex(-1));
    assertThrows(IndexOutOfBoundsException.class, () -> list.getByIndex(3));
  }

  @Test
  void removeByIndex() {
    list.add(1);
    list.add(2);
    list.add(3);
    assertEquals(2, list.removeByIndex(1));
    // 1 -> 3 -> 4
    list.add(4);
    assertEquals(1, list.removeByIndex(0));
    // 3 -> 4
    list.add(10);
    // 3 -> 4 -> 10
    assertEquals(10, list.removeByIndex(2));
    // 3 -> 4
    assertThrows(IndexOutOfBoundsException.class, () -> list.removeByIndex(-1));
    assertThrows(IndexOutOfBoundsException.class, () -> list.removeByIndex(3));
  }

  @Test
  void getHead() {
    list.add(1);
    list.add(2);
    list.add(3);
    assertEquals(1, list.getHead().getElement());
  }

  @Test
  void setHead() {
    list.add(1);
    list.add(2);
    list.add(3);
    list.setHead(new Node<>(10));
    assertEquals(10, list.getHead().getElement());
  }

  @Test
  void contains() {
    list.add(1);
    list.add(2);
    list.add(3);
    assertTrue(list.contains(1));
    assertTrue(list.contains(2));
    assertTrue(list.contains(3));
    assertFalse(list.contains(4));
  }
}
