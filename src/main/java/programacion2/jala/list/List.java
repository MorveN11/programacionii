package programacion2.jala.list;

import programacion2.jala.exceptions.EmptyListException;
import programacion2.jala.exceptions.NotFoundElementException;

public class List<T extends Comparable<T>> implements IList<T> {

  private Node<T> head;

  public List() {
    this.head = null;
  }

  public Node<T> getHead() {
    return head;
  }

  public void setHead(Node<T> head) {
    head.setNext(this.head.getNext());
    this.head = head;
  }

  private boolean indexIsNotValid(int index) {
    return index < 0 || index > size() - 1;
  }

  @Override
  public int size() {
    int i = 0;
    Node<T> current = head;
    while (current != null) {
      current = current.getNext();
      i++;
    }
    return i;
  }

  @Override
  public boolean isEmpty() {
    return head == null;
  }

  @Override
  public boolean contains(T element) {
    Node<T> current = head;
    while (current != null) {
      if (current.getElement().equals(element)) {
        return true;
      }
      current = current.getNext();
    }
    return false;
  }

  @Override
  public T add(T element) {
    Node<T> current = head;
    if (current == null) {
      head = new Node<>(element);
      return head.getElement();
    }
    while (current.getNext() != null) {
      current = current.getNext();
    }
    current.setNext(new Node<>(element));
    return current.getNext().getElement();
  }

  @Override
  public T addAtIndex(int index, T element) {
    if (index == 0) {
      Node<T> node = new Node<>(element);
      node.setNext(head);
      head = node;
      return head.getElement();
    }
    if (index == size()) {
      Node<T> current = head;
      while (current.getNext() != null) {
        current = current.getNext();
      }
      current.setNext(new Node<>(element));
      return current.getNext().getElement();
    }
    if (indexIsNotValid(index)) {
      throw new IndexOutOfBoundsException();
    }
    int i = 1;
    Node<T> prevCurrent = head;
    Node<T> current = head.getNext();
    while (current != null) {
      if (i == index) {
        Node<T> node = new Node<>(element);
        node.setNext(current);
        prevCurrent.setNext(node);
        return node.getElement();
      }
      prevCurrent = current;
      current = current.getNext();
      i++;
    }
    throw new NotFoundElementException();
  }

  @Override
  public T remove(T element) {
    Node<T> current = head;
    if (current == null) {
      throw new EmptyListException();
    }
    if (current.getElement().equals(element)) {
      T removedElement = current.getElement();
      head = current.getNext();
      return removedElement;
    }
    Node<T> previous = current;
    current = current.getNext();
    while (current != null) {
      if (current.getElement().equals(element)) {
        T removedElement = current.getElement();
        previous.setNext(current.getNext());
        return removedElement;
      }
      previous = current;
      current = current.getNext();
    }
    throw new NotFoundElementException();
  }

  @Override
  public T removeByIndex(int index) {
    if (indexIsNotValid(index)) {
      throw new IndexOutOfBoundsException();
    }
    if (head == null) {
      throw new EmptyListException();
    }
    if (index == 0) {
      T element = head.getElement();
      head = head.getNext();
      return element;
    }
    int i = 1;
    Node<T> prevCurrent = head;
    Node<T> current = head.getNext();
    while (current != null) {
      if (i == index) {
        T element = current.getElement();
        prevCurrent.setNext(current.getNext());
        return element;
      }
      prevCurrent = current;
      current = current.getNext();
      i++;
    }
    throw new NotFoundElementException();
  }

  @Override
  public T update(T element, T elementToUpdate) {
    Node<T> current = head;
    while (current != null) {
      if (current.getElement().equals(element)) {
        current.setElement(elementToUpdate);
        return element;
      }
      current = current.getNext();
    }
    throw new NotFoundElementException();
  }

  @Override
  public T updateByIndex(int index, T elementToUpdate) {
    if (indexIsNotValid(index)) {
      throw new IndexOutOfBoundsException();
    }
    int i = 0;
    Node<T> current = head;
    while (current != null) {
      if (i == index) {
        T element = current.getElement();
        current.setElement(elementToUpdate);
        return element;
      }
      current = current.getNext();
      i++;
    }
    throw new NotFoundElementException();
  }

  @Override
  public T get(T element) {
    Node<T> current = head;
    while (current != null) {
      if (current.getElement().equals(element)) {
        return current.getElement();
      }
      current = current.getNext();
    }
    throw new NotFoundElementException();
  }

  @Override
  public T getByIndex(int index) {
    if (indexIsNotValid(index)) {
      throw new IndexOutOfBoundsException();
    }
    int i = 0;
    Node<T> current = head;
    while (current != null) {
      if (i == index) {
        return current.getElement();
      }
      current = current.getNext();
      i++;
    }
    throw new NotFoundElementException();
  }

  @Override
  public int compareIndexes(int index1, int index2) {
    if (indexIsNotValid(index1) || indexIsNotValid(index2)) {
      throw new IndexOutOfBoundsException();
    }
    Node<T> current = head;
    Node<T> node1 = null;
    Node<T> node2 = null;
    int i = 0;
    while (current != null) {
      if (node1 != null && node2 != null) {
        break;
      }
      if (i == index1) {
        node1 = current;
      }
      if (i == index2) {
        node2 = current;
      }
      current = current.getNext();
      i++;
    }
    if (node1 == null || node2 == null) {
      throw new NotFoundElementException();
    }
    return node1.getElement().compareTo(node2.getElement());
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    Node<T> current = head;
    while (current != null) {
      stringBuilder.append(current).append(current.getNext() == null ? "" : "\n");
      current = current.getNext();
    }
    return """
            ------- List -------
            %s
            --------------------
            """.formatted(stringBuilder.toString());
  }
}
