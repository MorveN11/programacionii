package programacion2.jala.list;

public class Node<T> {

  private T element;

  private Node<T> next;

  public Node(T element) {
    this.element = element;
  }

  public Node() {
    this.element = null;
  }

  public T getElement() {
    return element;
  }

  public void setElement(T element) {
    this.element = element;
  }

  public Node<T> getNext() {
    return next;
  }

  public void setNext(Node<T> next) {
    this.next = next;
  }

  @Override
  public String toString() {
    return "Node: %s -> Node: %s"
            .formatted(element.toString(),
                    next == null ? null : next.getElement().toString());
  }
}
