package programacion2.jala.list;

public interface IList<T> {

  /**
   * Returns the size of the list
   *
   * @return the size of the list
   */
  int size();

  boolean isEmpty();

  boolean contains(T element);

  T add(T element);

  T addAtIndex(int index, T element);

  T remove(T element);

  T removeByIndex(int index);

  T update(T element, T elementToUpdate);

  T updateByIndex(int index, T elementToUpdate);

  T get(T element);

  T getByIndex(int index);

  int compareIndexes(int index1, int index2);
}
