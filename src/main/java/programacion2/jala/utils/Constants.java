package programacion2.jala.utils;

public class Constants {

  public static final String EMPTY_LIST_EXCEPTION = "This List is already Empty!";

  public static final String NOT_FOUND_EXCEPTION = "This element is not in the List!";
}
