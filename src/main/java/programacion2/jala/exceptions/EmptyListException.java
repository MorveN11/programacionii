package programacion2.jala.exceptions;

import static programacion2.jala.utils.Constants.EMPTY_LIST_EXCEPTION;

public class EmptyListException extends RuntimeException {

  public EmptyListException() {
    super(EMPTY_LIST_EXCEPTION);
  }
}
