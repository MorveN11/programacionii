package programacion2.jala.exceptions;

import static programacion2.jala.utils.Constants.NOT_FOUND_EXCEPTION;

public class NotFoundElementException extends RuntimeException {

  public NotFoundElementException() {
    super(NOT_FOUND_EXCEPTION);
  }
}
