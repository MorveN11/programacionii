package programacion2.jala.generics;

public abstract class Animal implements IAnimal {

  private final String name;

  public Animal(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public String caminar() {
    return "Caminando";
  }
}
