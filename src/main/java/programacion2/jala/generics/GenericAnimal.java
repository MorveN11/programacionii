package programacion2.jala.generics;

public class GenericAnimal<T extends Animal & Comparable<T>> {

  private T animal;

  public GenericAnimal(T animal) {
    this.animal = animal;
  }

  public T getAnimal() {
    return animal;
  }

  public void setAnimal(T animal) {
    this.animal = animal;
  }

  public String caminar() {
    return animal.caminar();
  }

  public int compareTo(T animal) {
    return this.animal.compareTo(animal);
  }
}
