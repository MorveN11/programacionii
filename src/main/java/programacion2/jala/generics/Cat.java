package programacion2.jala.generics;

public class Cat extends Animal implements Comparable<Cat> {

  public Cat(String name) {
    super(name);
  }

  @Override
  public int compareTo(Cat o) {
    return this.getName().compareTo(o.getName());
  }
}
